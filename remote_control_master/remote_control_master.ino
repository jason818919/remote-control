#include <ArduinoJson.h>
#include <math.h>
//global variables declarationss
int sensorXPin = A0; // analog pin 0
int sensorYPin = A1; // analog pin 1
double rmax_square = 547600;
double x,y,r,theta,sign,r_square,radian_left,radian_right,tmp_left,tmp_right,correction_left,correction_right;
double filter_sum_left,filter_sum_right;
double moving_average_left[32];
double moving_average_right[32];
int degree_left,degree_right;
String str;
StaticJsonBuffer<200> jsonBuffer;
JsonObject& obj = jsonBuffer.createObject();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200,SERIAL_8N1); // set the UARTs
  for(int i=0;i<32;i++){
    moving_average_left[i] = 90;
    moving_average_right[i] = 90;
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  x = analogRead(sensorXPin) - 512 ;
  y = analogRead(sensorYPin) - 501 ;
  y=-y;
  r_square = square(x)+square(y);
  r = sqrt(r_square);
  theta = atan2(y,x);
  if(r==0){
    if(theta==0){
      radian_left = (PI/2);
      radian_right = (PI/2);
    }else{
      sign = theta/fabs(theta);
      radian_left = (PI/2)*sign;
      radian_right = (PI/2)*sign;
    }
  }else{
    correction_left = (1+cos(theta)) / 2;
    correction_right = (1-cos(theta)) / 2;
    tmp_left = sqrt((rmax_square-r_square*correction_left)/(r_square*correction_left));
    tmp_right = sqrt((rmax_square-r_square*correction_right)/(r_square*correction_right));
    if(theta==0){
      radian_left = atan(tmp_left);
      radian_right = atan(tmp_right);
    }else{
      sign = theta/fabs(theta);
      radian_left = atan(tmp_left*sign) + (1-sign)*PI/2;
      radian_right = atan(tmp_right*sign) + (1-sign)*PI/2;
    }
  }
  radian_left = PI - radian_left;
  for(int i=0;i<31;i++){
    moving_average_left[i] = moving_average_left[i+1];
    moving_average_right[i] = moving_average_right[i+1];
  }
  moving_average_left[31] = radian_left*180/PI;
  moving_average_right[31] = radian_right*180/PI;
  filter_sum_left = 0;
  filter_sum_right = 0;
  for(int i=0;i<32;i++){
    filter_sum_left += moving_average_left[i];
    filter_sum_right += moving_average_right[i];
  }
  degree_left = filter_sum_left / 32;
  degree_right = filter_sum_right / 32;
  //obj["x"] = x;
  //obj["y"] = y;
  //obj["r"] = r;
  //obj["theta"] = theta;
  //obj["left_corr"] = correction_left;
  //obj["right_corr"] = correction_right;
  obj["wheel_left"] = degree_left;
  obj["wheel_right"] = degree_right;
  obj["ACK"] = "NONE";
  if(Serial.available()){
    str = Serial.readStringUntil('\n');
    if(str=="OK"){
      obj["ACK"]="OK";
    }else if(str=="FAIL"){
      obj["ACK"]="FAIL";
    }else{
      obj["ACK"]="SEG";
    }
  }
  obj.printTo(Serial);
  Serial.print("\n");
  delay(50);
}
