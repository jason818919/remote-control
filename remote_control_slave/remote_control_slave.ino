#include <ArduinoJson.h>
#include <Servo.h>
String str;
int wheel_left_degree,wheel_right_degree;
Servo wheel_left;
Servo wheel_right;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200,SERIAL_8N1);
  wheel_left_degree = 90;
  wheel_right_degree = 90;
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available()>0){
    str = Serial.readStringUntil('\n');
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& obj = jsonBuffer.parseObject(str);
    if(obj.success()){
      wheel_left_degree = obj["wheel_left"];
      wheel_right_degree = obj["wheel_right"];
      if(wheel_left_degree<95 && wheel_left_degree>85){
        if(wheel_left.attached()){
          wheel_left.detach();
        }
      }else{
        if(!wheel_left.attached()){
          wheel_left.attach(9);
        }
        wheel_left.write(wheel_left_degree);
      }
      if(wheel_right_degree<95 && wheel_right_degree>85){
        if(wheel_right.attached()){
          wheel_right.detach();
        }
      }else{
        if(!wheel_right.attached()){
          wheel_right.attach(10);
        }
        wheel_right.write(wheel_right_degree);
      }
      Serial.print("OK\n");
    }else{
      Serial.print("FAIL\n");
    }
  }
}
